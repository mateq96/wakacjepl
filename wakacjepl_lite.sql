 
CREATE DATABASE wakacjepl DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE wakacjepl;
 

CREATE TABLE busy (
  id int(11) NOT NULL primary key AUTO_INCREMENT,
  nazwa varchar(100) DEFAULT 'autokar',
  miejsca int(11) NOT NULL DEFAULT '0'
);  
 
CREATE TABLE cel (
  id int(11) NOT NULL primary key AUTO_INCREMENT,
  nazwa varchar(100) NOT NULL DEFAULT 'Wakacje niespodzianka',
  cena decimal(10,0) NOT NULL,
  czas int(11) NOT NULL DEFAULT '7'
);  
 
CREATE TABLE hotele (
  id int(11) NOT NULL primary key AUTO_INCREMENT,
  nazwa varchar(200) NOT NULL,
  gwiazdki int(11) NOT NULL,
  basen tinyint(1) NOT NULL DEFAULT '0',
  wolne int(11) NOT NULL DEFAULT '0' COMMENT 'liczba wolny miejc'
); 
 
CREATE TABLE kierowca (
  id int(11) NOT NULL primary key AUTO_INCREMENT,
  imie varchar(20) NOT NULL,
  nazwisko varchar(20) NOT NULL
); 
 
CREATE TABLE klienci (
  id int(11) NOT NULL primary key AUTO_INCREMENT,
  name varchar(255) NOT NULL,
  telefon int(11) NOT NULL,
  uwagi varchar(255) NOT NULL
); 
 
CREATE TABLE logi (
  kod varchar(10) NOT NULL primary key,
  blad varchar(255) NOT NULL,
  komunikat varchar(255) NOT NULL DEFAULT 'twoj problem jest rozwiazywany'
); 
 
CREATE TABLE platnosci (
  id_k int(11) NOT NULL REFERENCES klienci (id) ON DELETE NO ACTION ON UPDATE CASCADE,
  id_c int(11) NOT NULL REFERENCES cel (id) ON DELETE NO ACTION ON UPDATE CASCADE,
  PRIMARY KEY (id_k,id_c)
); 
 
CREATE TABLE poziomy (
  indeks int(11) NOT NULL primary key AUTO_INCREMENT,
  nazwa varchar(20) NOT NULL UNIQUE
); 
 
CREATE TABLE transport (
  id int(11) NOT NULL primary key AUTO_INCREMENT,
  id_k int(11) NOT NULL REFERENCES kierowca (id) ON UPDATE CASCADE,
  id_b int(11) NOT NULL REFERENCES busy (id) ON UPDATE CASCADE
) ;
 
CREATE TABLE wycieczka (
  id int(11) NOT NULL UNIQUE AUTO_INCREMENT,
  id_k int(11) NOT NULL REFERENCES klienci (id) ON UPDATE CASCADE,
  id_c int(11) NOT NULL REFERENCES cel (id) ON UPDATE CASCADE,
  id_t int(11) NOT NULL REFERENCES transport (id) ON UPDATE CASCADE,
  id_h int(11) DEFAULT NULL REFERENCES hotele (id) ON DELETE NO ACTION ON UPDATE CASCADE,
  data_w date DEFAULT NULL,
  PRIMARY KEY (id_k,id_c)
); 
  
CREATE TABLE zagrozenie (
  id_c int(11) NOT NULL REFERENCES cel (id) ON DELETE CASCADE ON UPDATE CASCADE,
  nazwa varchar(100) NOT NULL,
  opis varchar(255) NOT NULL,
  poziom int(11) DEFAULT NULL REFERENCES poziomy (indeks) ON DELETE SET NULL ON UPDATE CASCADE,
  PRIMARY KEY (id_c,nazwa)
); 

 