use wakacjepl ;
/*procedura walidacji wprowadzanych wycieczek

--sprawdza wolne miejsca w autokarze i hotelu oraz czy kierowca nie jest w trasie tym autokarem w tym czasie

--wymaga podania w kolejności id_celu, id_klienta, id_hotelu, id_transportu, date_wyjazdu

--DROP PROCEDURE  `new_wycieczka`;
*/
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `new_wycieczka`(IN `cel` INT, IN `klient` INT, IN `hotel` INT, IN `transport` INT, IN `data_wyjazdu` DATE) NOT DETERMINISTIC CONTAINS SQL SQL SECURITY DEFINER begin
	declare lozka int(11);
	declare fotele int(11);
	declare dni int(11);
	 
	if year(data_wyjazdu) not between year(now()) and year(now())+3 then
		select"popraw datę";
		signal sqlstate '45000' set message_text ="data musi być w formacie RRRR.MM.DD i nie przekraczać 3 lata";
		
	end if;
	select wolne into lozka from hotele where id=hotel;
	if lozka<= (select count(*) from wycieczka where id_c=cel and id_h=hotel) then
		select"poszukaj wolnego hotelu";
		signal sqlstate '45000' set message_text ="Brak miejsc w tym hotelu";
		
	else
		select miejsca into fotele from busy where id=(select id_b from transport where id=transport);
		if fotele<=(select count(*) from wycieczka where id_c=cel and id_t=transport) then
			select"poszukaj wolnego autokaru";
			signal sqlstate '45000' set message_text ="Brak miejsc w tym autokarze";
			
		else
			select czas into dni from cel where id=cel;
			if 0< (select count(*) from wycieczka  where id_t=transport and data_w between data_wyjazdu and date_add(data_wyjazdu,interval dni day) ) then
				select"poszukaj wolnego kierowce";
				signal sqlstate '45000' set message_text ="ten transport jest już w urzyciu";
				
			else
				insert into wycieczka values(null,klient,cel,transport,hotel,DATE_FORMAT(data_wyjazdu, "%Y-%m-%d"));
				select"dodano z sukcesem";
			end if;	
        end if;	
	end if;
	
END;
//

--procedura dodawania platnosci
--trzeba podac id_klienta, id_celu wyceczki i kwote jaką wplaca klient
--kwota jest sprawdzana, trzeba wplacac rowne kwoty
DROP PROCEDURE `new_platnosc`;
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `new_platnosc`(IN `klient` INT(11), IN `miejsce` INT(11), IN `kwota` INT(11)) NOT DETERMINISTIC NO SQL SQL SECURITY DEFINER BEGIN
	declare dlug int(11) DEFAULT 0;
	declare licznik int(11);
	declare komunikat varchar(250) DEFAULT "ok";
	if 0=(select count(*) from wycieczka where id_c=miejsce and id_k=klient) then
		signal sqlstate '45000' set message_text ="Klient nie ma takiej wycieczki";
	end if;
	if 0<(select COUNT(*) from platnosci WHERE id_c=miejsce and id_k=klient) then
		SELECT "klient opłacił tą wycieczkę." into komunikat  ;
	else
		select cena into dlug from cel where id=miejsce;
		if dlug=kwota then
			INSERT into platnosci values(klient,miejsce);
			select "zaksięgowano wpłate" into komunikat;
		ELSE
			select concat("Kwota niepoprawna, pobierz od klienta ",dlug,"zł") into komunikat;
		end if;
	end  if; 
	if 0=(select COUNT(cena) as dlug from cel where cel.id not in(select id_c from platnosci where id_k=klient and id_c=miejsce) and cel.id in(select id_c from wycieczka where id_k=klient)) THEN
		select 0 into dlug;
	else
		select sum(cena) into dlug from cel where cel.id not in(select id_c from platnosci where id_k=klient and id_c=miejsce) and cel.id in(select id_c from wycieczka where id_k=klient);
	end if;     
	select concat(komunikat," klienta ma jeszcze do zaplaty: ",dlug,"zł");
	 
end
//

--funkcja prognozy pogody generuje link do prognozy całorocznej w danym miejscu, wystarczy podać id celu
DROP FUNCTION  `pogoda`;
DELIMITER //
CREATE DEFINER=`root`@`localhost` FUNCTION `pogoda`(`cel` INT) RETURNS VARCHAR(255) NOT DETERMINISTIC CONTAINS SQL SQL SECURITY DEFINER begin
declare miejsce varchar(50);
select nazwa into miejsce from cel where id=cel;

return concat("https://www.travelplanet.pl/przewodnik/",miejsce,"/temperatury/") ;
              
 end;
 //
 
--funkcja wyświetlająca wszystkie szczegóły  wycieczek, wystajczy podać id wycieczki
 DROP FUNCTION  `info`;
 DELIMITER //
CREATE DEFINER=`root`@`localhost` FUNCTION `info`(`id_w` INT) RETURNS VARCHAR(255) NOT DETERMINISTIC READS SQL DATA SQL SECURITY DEFINER BEGIN
declare basen1 varchar(20);
declare zagrozenie1 varchar(250);
declare oplata varchar(20);
select hotele.basen into basen1 from wycieczka,hotele where wycieczka.id=id_w and hotele.id=wycieczka.id_h;
if 0<basen1 THEN
select " z basenem" into basen1;
ELSE
select " bez basenu" into basen1;
end if; 

select poziom into zagrozenie1 from zagrozenie,wycieczka where wycieczka.id=id_w and zagrozenie.id_c=wycieczka.id_c;
if zagrozenie1=0 THEN
select "nie ma zagrozen" into zagrozenie1;
else 
select concat("zagrozenie ",poziomy.nazwa,": ",zagrozenie.nazwa) into zagrozenie1 from zagrozenie,wycieczka, poziomy where wycieczka.id=id_w and zagrozenie.id_c=wycieczka.id_c and poziomy.indeks=zagrozenie.poziom ;
end if;

select COUNT(*) into oplata from wycieczka,platnosci where wycieczka.id=id_w and platnosci.id_c=wycieczka.id_c and platnosci.id_k=wycieczka.id_k;
if 0<oplata THEN
select " oplacono" into oplata;
else
select " nie wplacone" into oplata;
end if;

return concat(
    "klient ",(select name from wycieczka,klienci where wycieczka.id=id_w and klienci.id=wycieczka.id_k)," jedzie autokarem ",(select nazwa from wycieczka,busy,transport where wycieczka.id=id_w and busy.id=transport.id_b and transport.id=wycieczka.id_t)," prowadzonym przez ",(select concat(imie," ", nazwisko) as name from wycieczka,kierowca,transport where wycieczka.id=id_w and transport.id=wycieczka.id_t and kierowca.id=transport.id_k)," na ",(select czas from wycieczka,cel where wycieczka.id=id_w and cel.id=wycieczka.id_c)," dniową wycieczkę do celu ",(select nazwa from wycieczka,cel where wycieczka.id=id_w and cel.id=wycieczka.id_c)," (",zagrozenie1,")"," z noclegiem w ",(select gwiazdki from wycieczka,hotele where wycieczka.id=id_w and hotele.id=wycieczka.id_h)," gwazdkowym hotelu ",(select nazwa from wycieczka,hotele where wycieczka.id=id_w and hotele.id=wycieczka.id_h),basen1," koszt PLN ",(select cena from wycieczka,cel where wycieczka.id=id_w and cel.id=wycieczka.id_c),oplata
    );
end;
//